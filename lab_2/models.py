from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=1000)
    sender = models.CharField(max_length=1000)
    title = models.CharField(max_length=1000)
    message = models.TextField()
    # DONE Implement missing attributes in Note model
