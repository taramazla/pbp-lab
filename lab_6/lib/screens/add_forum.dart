import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';

// ignore: camel_case_types
class Add_Forum extends StatefulWidget {
  const Add_Forum({Key? key, required this.title}) : super(key: key);
  static const routeName = '/forum';

  final String title;

  @override
  _ForumPageState createState() => _ForumPageState();
}

class _ForumPageState extends State<Add_Forum> {
  final _formKey = GlobalKey<FormState>();
  final categories = ['General Discussion', 'Covid Info', 'Drug Info'];
  String? value;
  DropdownMenuItem<String> buildMenuItem(String value) =>
      DropdownMenuItem<String>(
        value: value,
        child: Text(
          value,
          style: const TextStyle(color: Colors.white),
        ),
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // set it to false
      appBar: AppBar(
        title: const Text("Add Forum"),
        backgroundColor: const Color.fromRGBO(27, 27, 27, 1),
      ),
      drawer: const MainDrawer(),
      body: Form(
        key: _formKey,
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              const Text(
                'Discussion',
                style: TextStyle(fontSize: 20),
              ),
              const Text(
                'by user1',
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 20),
              // TextField(),
              TextFormField(
                decoration: InputDecoration(
                  hintText: "Title Discussion",
                  labelText: "Title Discussion",
                  icon:
                      const Icon(IconData(63389, fontFamily: 'MaterialIcons')),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please fill out this field.';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 10),
              DropdownButton<String>(
                hint:const Text("*Choose Category Discussion*",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500),),
                value: value,
                items: categories.map((String value) {
                  return buildMenuItem(value);
                }).toList(),
                onChanged: (value) => setState(() {
                  this.value = value;
                }),
                
              ),
              const SizedBox(height: 10),
              TextFormField(
                decoration: InputDecoration(
                  hintText: "Write Here",
                  labelText: "Write Here",
                  icon:
                      const Icon(IconData(61621, fontFamily: 'MaterialIcons')),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please fill out this field.';
                  }
                  return null;
                },
              ),
              Container(
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                    color: const Color(0xff6B46C1),
                    borderRadius: BorderRadius.circular(20)),
                // ignore: deprecated_member_use
                child: FlatButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                  child: const Text(
                    "Post to Forum",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
