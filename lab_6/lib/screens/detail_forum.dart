import 'package:flutter/material.dart';


class Details extends StatelessWidget {
  const Details({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context){
     return Scaffold(
       backgroundColor: const Color(0xff131313),
       appBar: AppBar(
          title: const Text('Forum'),
          backgroundColor: const Color(0xff131313),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            Container(
              child: 
              const Text('iniTitle',textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 35,
              fontWeight: FontWeight.bold),
              ),
              margin: const EdgeInsets.fromLTRB(0, 25, 0, 20),
            ),
            Container(
              child: 
              const Text('Post on 15 Nov 2021',
              textAlign: TextAlign.right,
              style: 
              TextStyle(fontSize: 20,
              ),
              ),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 15),
            ),
            Container(
              child: 
              const Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
              textAlign: TextAlign.justify,
              style: 
              TextStyle(fontSize: 20,
              ),
              ),
              margin: const EdgeInsets.fromLTRB(10, 20, 10, 10),
            ),
            Container(
              child: 
              const Text('Made by user1',
              textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 20,
              fontWeight: FontWeight.bold
              ),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            ])
       );
   }
}
