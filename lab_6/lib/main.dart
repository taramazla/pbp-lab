import 'package:flutter/material.dart';
import 'package:lab_6/screens/add_forum.dart';
import 'package:lab_6/screens/detail_forum.dart';
import 'package:lab_6/widgets/main_drawer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'Covid Consult';
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        darkTheme: ThemeData(brightness: Brightness.dark, primarySwatch: Colors.purple),
        themeMode: ThemeMode.dark, 
        title: title,
        home: const MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  // ignore: use_key_in_widget_constructors
  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const MainDrawer(),
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: const Color(0xff131313),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            Container(
              child: 
              const Text('Forum',textAlign: TextAlign.center,
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            
            Container(
              child: 
              const Text('Search Forum',textAlign: TextAlign.center,
              style: 
                TextStyle(fontSize: 30,
                ),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Search'),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(color: const Color(0xff6B46C1),
              borderRadius: BorderRadius.circular(20)),  
              // ignore: deprecated_member_use
              child: FlatButton( 
                onPressed: () { 
                Navigator.push(context,MaterialPageRoute(builder: (_) => const Add_Forum(title: '',)));  
                },
                child: 
                  const Text( 'Start Discussion',style: TextStyle(color: Colors.white, fontSize: 25),
                  ), 
              ),    
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          ),         
        ],
        ),
      //   bottomNavigationBar: BottomNavigationBar(
        
      //   backgroundColor: const Color(0xff131313),
      //   unselectedItemColor: Colors.white,
      //   selectedItemColor: Colors.white,
        
        
      //   // type: BottomNavigationBarType.fixed,
      //   items: [
      //     BottomNavigationBarItem(
      //       backgroundColor: Theme.of(context).primaryColor,
      //       icon: const Icon(Icons.home),
      //       title: Text('Home'),
      //     ),
      //     BottomNavigationBarItem(
      //       backgroundColor: Theme.of(context).primaryColor,
      //       icon: Icon(Icons.article),
      //       title: const Text('Forum'),
      //     ),
          
      //   ],
      // ),
      );
      
      Widget buildSearch() {
        return const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        ); 
      }
      
      Widget buildColoredCard() => Card(
        shadowColor: Colors.red,
        elevation: 8,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Color.fromRGBO(13, 13, 13, 1), Color(0xff131313)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Judul Di main',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 20),
              const Text(
                "isi dimainn...",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
              
              ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: (){Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Details()),
                    );}, 
                  child: const Text(
                  'Readmore →',
                  ),
                ),
              ],
             )
            ],
          ),
        ),
      );
}