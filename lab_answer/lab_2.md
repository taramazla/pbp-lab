# Assignment lab_2

## 1. Apakah perbedaan antara JSON dan XML?

| JSON (JavaScript Object Notation)                                                   |XML (eXtensible Markup Language)                                               |
| :----:                                                                              | :----:                                                                        |
| JSON didasarkan pada bahasa pemrograman JavaScript yang berorientasi pada data      | XML merupakan bahasa markup yang berorientasi pada dokumen                    |
| Mendukung Array                                                                     | Tidak mendukung Array                                                         |
| Mendukung pembuatan objek                                                           | Dibuat manual oleh program                                                    |
| Sintaks pada JSON lebih pendek                                                      | Sintaks pada XML lebih panjang sehingga memiliki ukuran data yang lebih besar |
| Hanya mendukung objek bertipe data primitif                                         | Mendukung tipe data non primitif                                              |
| Berjalan sangat cepat karena ukuran file kecil                                      | Berjalan lebih lambat karena ukuran file besar                                |
| Mendukung UTF dan ASCII                                                             | Mendukung UTF-8 dan UTF-16                                                    |
| Tidak ada ketentuan untuk namespace                                                 | Mendukung namespace                                                           |
| Data disimpan dalam benuk pasangan key dan value                                    | Data disimpan sebagai tree structure                                          |

## 2. Apakah perbedaan antara HTML dan XML?

| HTML (Hypertext Markup Language)                                                    |XML (eXtensible Markup Language)                                               |
| :----:                                                                              | :----:                                                                        |
| HTML memiliki fokus kepada penyajian data                                           | XML memiliki fokus kepada transfer data                                       |
| Tidak bersifat *Case Sensitive* sehingga besar atau kecil tiap huruf dan kalimatnya tidak berpengaruh| Bersifat *Case Sensitive* sehingga huruf besar atau kecil tiap huruf dan kalimatnya sangat berpengaruh.|
|Tidak terlalu ketat pada tag penutup                                                 |Sangat ketat dalam memperhatikan tag penutup                                   |
| Memiliki tag yang sudah ditentukan sebelumnya, seperti : `<table> <h1> <p>`, dan lain sebagainya|tag pada XML biasanya tidak dapat ditentukan sebelumnya            |

## Referensi

1. <https://www.monitorteknologi.com/perbedaan-json-dan-xml/>
2. <https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html>
