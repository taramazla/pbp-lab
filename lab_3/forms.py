from lab_1.models import Friend
from django.forms import ModelForm, DateInput, widgets

# From : https://www.youtube.com/watch?v=I2-JYxnSiB0
class InputDate(DateInput):
    input_type = 'date'

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        # ['name', 'npm', 'DOB']
        widgets = {'birth_date':InputDate()}



