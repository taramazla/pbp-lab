import 'package:flutter/material.dart';
import 'package:lab_7/screens/add_forum.dart';
import 'package:lab_7/screens/detail_forum.dart';
import 'package:lab_7/widgets/main_drawer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  static const String title = 'Covid Consult';
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        darkTheme: ThemeData(brightness: Brightness.dark, primarySwatch: Colors.purple),
        themeMode: ThemeMode.dark, 
        title: title,
        home: const MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  // ignore: use_key_in_widget_constructors
  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final searchBy = ['All', 'Title', 'Content','Author'];
  String? value;
  DropdownMenuItem<String> buildMenuItem(String value) =>
      DropdownMenuItem<String>(
        value: value,
        child: Text(
          value,
          style: const TextStyle(color: Colors.white),
        ),
      );
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const MainDrawer(),
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: const Color(0xff131313),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            Container(
              child: 
              const Text('Forum',textAlign: TextAlign.center,
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            DropdownButtonFormField<String>(
                isExpanded: true,
                value: 'All',
                items: searchBy.map((String value) {
                  return buildMenuItem(value);
                }).toList(),
                onChanged: (value) => setState(() {
                  this.value = value;
                }),
                validator: (value) => value == null ? 'Please fill out this field.' : null,
              ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Search'),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // ignore: deprecated_member_use
                    FlatButton(
                        onPressed: () {},
                        child: const Icon(
                          Icons.category_rounded
                        )),
                    // ignore: deprecated_member_use
                    FlatButton(
                        onPressed: () {},
                        child: const Icon(
                          Icons.graphic_eq_outlined,
                        )),
                    // ignore: deprecated_member_use
                    FlatButton(
                        onPressed: () {},
                        child: const Icon(
                          Icons.verified_user_sharp
                        )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const <Widget>[
                    Text(
                      'All Category',
                    ),
                    Text(
                      'General Discussion',
                    ),
                    Text(
                      'Covid Info     ',
                    ),
                  ],
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // ignore: deprecated_member_use
                    FlatButton(
                        onPressed: () {},
                        child: const Icon(
                          Icons.donut_large_outlined,
                        )),
                    // ignore: deprecated_member_use
                    FlatButton(
                        onPressed: () {},
                        child: const Icon(
                          Icons.supervised_user_circle_sharp,
                        )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: const <Widget>[
                    Text(
                      '      Drug Info',
                    ),
                    Text(
                      '  My Discussion',
                      style: TextStyle(letterSpacing: 2.0, color: Colors.red),
                    ),
                  ],
                ),
              ],
            ),
            // Container(
            //   child: 
            //   const Text('Search Forum',textAlign: TextAlign.center,
            //   style: 
            //     TextStyle(fontSize: 30,
            //     ),
            //   ),
            //   margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            // ),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(color: const Color(0xff6B46C1),
              borderRadius: BorderRadius.circular(20)),  
              // ignore: deprecated_member_use
              child: FlatButton( 
                onPressed: () { 
                Navigator.push(context,MaterialPageRoute(builder: (_) => const Add_Forum(title: '',)));  
                },
                child: 
                  const Text( 'Start Discussion',style: TextStyle(color: Colors.white, fontSize: 25),
                  ), 
              ),    
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
          ),         
        ],
        ),
      );
      
      Widget buildSearch() {
        return const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        ); 
      }
      
      Widget buildColoredCard() => Card(
        shadowColor: Colors.red,
        elevation: 8,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Color.fromRGBO(13, 13, 13, 1), Color(0xff131313)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Judul Di main',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 20),
              const Text(
                "isi dimainn...",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
              
              ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                // ignore: deprecated_member_use
                FlatButton(
                  onPressed: (){Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Detail_Forum()),
                    );}, 
                  child: const Text(
                  'Readmore →',
                  ),
                ),
              ],
             )
            ],
          ),
        ),
      );
}