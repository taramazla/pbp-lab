import 'package:flutter/material.dart';
import 'package:lab_7/widgets/main_drawer.dart';

// ignore: camel_case_types
class Add_Forum extends StatefulWidget {
  const Add_Forum({Key? key, required this.title}) : super(key: key);
  static const routeName = '/forum';

  final String title;

  @override
  _ForumPageState createState() => _ForumPageState();
}

class _ForumPageState extends State<Add_Forum> {
  final _formKey = GlobalKey<FormState>();  
  void _processData() {
  // Process your data and upload to server
  _formKey.currentState?.reset();
  }
  final categories = ['General Discussion', 'Covid Info', 'Drug Info'];
  String? value;
  DropdownMenuItem<String> buildMenuItem(String value) =>
      DropdownMenuItem<String>(
        value: value,
        child: Text(
          value,
          style: const TextStyle(color: Colors.white),
        ),
      );
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false, // set it to false
      appBar: AppBar(
        title: const Text("Add Forum"),
        backgroundColor: const Color.fromRGBO(27, 27, 27, 1),
      ),
      drawer: const MainDrawer(),
      body: Form(
        key: _formKey,
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children:<Widget> [
              const Text(
                'Discussion',
                style: TextStyle(fontSize: 20),
              ),
              const Text(
                'by user1',
                style: TextStyle(fontSize: 20),
              ),
              const SizedBox(height: 20),
              // TextField(),
              TextFormField(
                decoration: InputDecoration(
                  hintText: "Title Discussion",
                  labelText: "Title Discussion",
                  // icon:
                  //     const Icon(IconData(63389, fontFamily: 'MaterialIcons')),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please fill out this field.';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 20),
              DropdownButtonFormField<String>(
                isExpanded: true,
                hint:const Text("*Choose Category Discussion*",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500),),
                value: value,
                items: categories.map((String value) {
                  return buildMenuItem(value);
                }).toList(),
                onChanged: (value) => setState(() {
                  this.value = value;
                }),
                validator: (value) => value == null ? 'Please fill out this field.' : null,
              ),
              const SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  hintText: "Write Here",
                  labelText: "Write Here",
                  // icon: const Icon(IconData(61621, fontFamily: 'MaterialIcons')),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
                maxLines: 6,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please fill out this field.';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 20),
              Padding(padding: const EdgeInsets.all(3),
                child:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // ignore: deprecated_member_use
                    RaisedButton(
                      child: const Text("Post to Forum"),
                      onPressed: (){
                        if (_formKey.currentState!.validate()) {
                          showDialog(
                            context: context, 
                            builder: (_) => const AlertDialog(
                              title: Text("Your article has been publish"),
                            ),
                            
                          );
                          _processData();    
                          // ignore: avoid_print
                          print("success");                  
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(builder: (context) => const MainPage(title: "main")),
                          // );
                        }
                      },
                      color: const Color(0xff6B46C1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                        side: const BorderSide(color: Color(0xff6B46C1),)
                      ),
                    ),
                  ],
                ),
                ),
            ],
          ),

        ),
      ),
    );
  }
}
