import 'package:flutter/material.dart';


// ignore: camel_case_types
class Detail_Forum extends StatefulWidget {
  const Detail_Forum({Key? key}) : super(key: key);
  @override
  _DetailForumState createState() => _DetailForumState();


}
class _DetailForumState extends State<Detail_Forum>{
    final _formKey = GlobalKey<FormState>();  
  void _processData() {
  // Process your data and upload to server
  _formKey.currentState?.reset();
  }
  @override
  Widget build(BuildContext context){
     return Scaffold(
       backgroundColor: const Color(0xff131313),
       appBar: AppBar(
          title: const Text('Forum'),
          backgroundColor: const Color(0xff131313),
        ),
        body: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            Container(
              child: 
              const Text('iniTitle',textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 35,
              fontWeight: FontWeight.bold),
              ),
              margin: const EdgeInsets.fromLTRB(0, 25, 0, 3),
            ),
            Container(
              child: 
              const Text('user1',
              textAlign: TextAlign.center,
              style: 
                TextStyle(fontSize: 20,
                fontWeight: FontWeight.bold
                ),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: 
              const Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
              textAlign: TextAlign.justify,
              style: 
              TextStyle(fontSize: 20,
              ),
              ),
              margin: const EdgeInsets.fromLTRB(10, 20, 10, 10),
            ),
            Container(
              child: 
              const Text('Comment Here',
              textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 20,
              fontWeight: FontWeight.bold
              ),
              ),
              margin: const EdgeInsets.fromLTRB(0, 20, 0,0),
            ),
            const SizedBox(height: 10),
            Form(
              key: _formKey,
              child: Container(   
              padding: const EdgeInsets.all(20),        
              child: Column(
                children:<Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "Title Discussion",
                      labelText: "Title Discussion",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please fill out this field.';
                      }
                      return null;
                    },
                    maxLines: 3,
                  ),
                  const SizedBox(height: 20),
               Padding(padding: const EdgeInsets.all(3),
                child:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // ignore: deprecated_member_use
                    RaisedButton(
                      child: const Text("Post to Forum"),
                      onPressed: (){
                        if (_formKey.currentState!.validate()) {
                          showDialog(
                            context: context, 
                            builder: (_) => const AlertDialog(
                              title: Text("Your comment has been publish"),
                            ),
                            
                          );
                          _processData();    
                          // ignore: avoid_print
                          print("success");                  
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(builder: (context) => const MainPage(title: "main")),
                          // );
                        }
                      },
                      color: const Color(0xff6B46C1),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                        side: const BorderSide(color: Color(0xff6B46C1),)
                      ),
                    ),
                  ],
                ),
                ),

                ],
              ),
              ),
            ),
            ]),
       );
   }

}
