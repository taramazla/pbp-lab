from django.shortcuts import render,HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all() # implement notes
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
        context ={}
        # create object of form
        form = NoteForm(request.POST or None)
      
        # check if form data is valid
        if form.is_valid() and request.method == 'POST':
            # save the form data to model
            form.save()
            return HttpResponseRedirect('/lab-4')  # Redirect on finish
        response = {'form': form}
        return render(request, "lab4_form.html", response)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)


