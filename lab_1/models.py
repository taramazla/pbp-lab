from django.db import models

# DONE Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.BigIntegerField()
    birth_date = models.DateField()
    # DONE Implement missing attributes in Friend model
